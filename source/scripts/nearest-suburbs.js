/**
* Suburb lookup Async
* 
* Handles the lookup of a subrub from the Express JS instance on the server.
* This method manages the front-end changes that appear as users type in their
* desired suburb.
* 
* @author Kashi Samaraweera <kashi@kashis.com.au>
*/

var com = com || {};
com.kashis = com.kashis || {};
com.kashis.geo = com.kashis.geo || {};

com.kashis.geo.nearestSuburbs = (function(){

    var NearestSuburbsAPI = {},
        $ = jQuery;
    
    var _controls = {
        suburbInput : 'input[name="suburb"]',
        asyncForm : '.Form--asyncHook',
        formSubmitBtn : 'input[type="submit"]',
        resultsTable : 'table.SuburbResults'
    };
    
    var TYPING_TIMEOUT = 1000, 
        SUBURB_MATCH_URI = 'http://dev.kashis.com.au/v1.0/nearest-suburb/match';
    
    /**
     * Initialises our page and binds our interactive controls to objects.
     */
    function _init() {
        for (var controlName in _controls)
            if (_controls.hasOwnProperty(controlName))
                _controls[controlName] = $(_controls[controlName]);
        
        _controls.suburbInput.on('keypress', _awaitFinishTyping);
        _controls.asyncForm
            .removeClass('Form--asyncHook')
            .addClass('Form--async');
    }
    
    /**
     * Creates a timeout event for an input upon key press. This methd will
     * cancel any upcoming searches (scheduled using setTimeout) and extend the
     * waiting period by the number of milliseconds defined in TYPING_TIMEOUT.
     * 
     * If no pending search operation exists then one is created automatically.
     * @param keyPressEvent Event   A jQuery-wrapped Event object that is used
     *                              to specify the form input field on which
     *                              we're waiting.
     */
    function _awaitFinishTyping(keyPressEvent) {
        var formInput = $(keyPressEvent.currentTarget),
            lastPressTimerId = _controls.suburbInput.data('last-press-timer-id'),
            fieldValue = formInput.val(),
            userKeyPressTimerId;

        if (lastPressTimerId !== undefined)
            clearTimeout(lastPressTimerId);
        
        if (fieldValue === "")
            return;
        
        userKeyPressTimerId = setTimeout(NearestSuburbsAPI.search, TYPING_TIMEOUT);
        formInput.data('last-press-timer-id', userKeyPressTimerId);      
    }
    
    /**
     * Performs a lookup on the server for matching near-by suburbs.
     */
    NearestSuburbsAPI.search = function() {
        var suburbQuery = _controls.suburbInput.val();
        if (suburbQuery === undefined)
            return;
        
        if (suburbQuery.length < 2)
            return;
        
        $.ajax(
            {
                url: SUBURB_MATCH_URI + '?suburb=' + suburbQuery,
                complete: _suburbQueryResponse
            }
        );
        _populateList({});
    };

    /**
     * Processes the response from the suburb query.
     * @param jqXhr
     * @private
     */
    function _suburbQueryResponse(jqXhr) {
        var status = jqXhr.status,
            suburbs = jqXhr.responseJSON;
        
        if (status !== 200) {
            _controls.asyncForm.addClass('Form--error');
        }
        
        _populateList(suburbs);
        
    }
    
    /**
     * Populates the results table with results.
     * @param suburbResults
     * @private
     */
    function _populateList(suburbResults) {
        var resultsTable = _controls.resultsTable,
            tbody = resultsTable.find('tbody');
        
        tbody.html('');
        
        if (! Array.isArray(suburbResults))
            return;

        suburbResults.forEach(
            function(suburbRow) {
                var suburbData = [],
                    suburbTableRow;

                suburbData.push(suburbRow.suburb);
                suburbData.push(suburbRow.postcode);
                suburbData.push(suburbRow.state);
                suburbData.push( suburbRow.latitude + ', ' + suburbRow.longitude);

                suburbTableRow = suburbData.join('</td><td>');
                tbody.append($('<tr><td>' + suburbTableRow + '</td></tr>'));
            }
        );
    }
    
    $(document).ready(_init);
    
    return NearestSuburbsAPI;
}());