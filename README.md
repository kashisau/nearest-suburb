# Nearest Suburb Lookup for Australian Suburbs

This is a front-end for an Australian postcode/suburb lookup that will return a list of suburbs based on their proximity to a given suburb.
It's currently in a proof-of-concept stage.

## Building

This is a static website that uses Gulp on (Node.js) for its build process. You'll need Node.js installed, and upon downloading the source, the website will build using:

```
gulp build
```

A `--production` flag may be used to build for deployment, where all sources are minified and source-mapped to an online repository.